from sklearn.neural_network import MLPClassifier

# setup
with open(r"iris-training.txt") as training_set:
    training_set = training_set.readlines()

with open(r"iris-test.txt") as test_set:
    test_set = test_set.readlines()

training_measurements = []
training_names = []

test_measurements = []
test_names = []

# parsing in the data

for line in training_set:
    arr = line.split()

    if len(arr) == 0:
        continue

    training_measurements.append([float(arr[0]), float(arr[1]), float(arr[2]), float(arr[3])])
    training_names.append(arr[4])

for line in test_set:
    arr = line.split()

    if len(arr) == 0:
        continue

    test_measurements.append([float(arr[0]), float(arr[1]), float(arr[2]), float(arr[3])])
    test_names.append(arr[4])

# outputs: ----------------------


def loop_over_different_options(hidden, random, data, output, score_data, score_output, training):
    average = 0
    xhidden = -1
    xrandom = -1

    for i in range(4, hidden):
        for j in range(8, random):
            clf = MLPClassifier(solver='sgd',
                                learning_rate='adaptive',
                                alpha=1e-5,
                                hidden_layer_sizes=(i,),
                                random_state=j,
                                verbose=False,
                                max_iter=1249,
                                learning_rate_init=0.2
                                )

            clf.fit(data, output)
            score = clf.score(score_data, score_output, sample_weight=None)

            if score > average:
                average = score
                xrandom = j
                xhidden = i

    print("training on the " + str(training) + " data with " + str(xhidden)
          + " hidden layers, and " + str(xrandom) + " being the random state gave the accuracy ---> " + str(average))


loop_over_different_options(39, 19, training_measurements, training_names, test_measurements, test_names, "training")
loop_over_different_options(39, 19, test_measurements, test_names, training_measurements, training_names, "test")
